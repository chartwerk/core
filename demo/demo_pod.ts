import {
  ChartwerkPod,
  Serie,
  Options
} from '../dist/index';

import * as d3 from 'd3';
import * as _ from 'lodash';


class DemoPod extends ChartwerkPod<Serie, Options> {
  lineGenerator = null;

  constructor(
    _el: HTMLElement,
    _series: Serie[] = [],
    _options: Options = {},
  ) {
    super(_el, _series, _options);
  }

  override renderMetrics(): void {
    this.clearAllMetrics();
    this.initLineGenerator();
    for(const serie of this.series.visibleSeries) {
      this.renderLine(serie);
    }
  }

  clearAllMetrics(): void {
    // TODO: temporary hack before it will be implemented in core.
    this.chartContainer.selectAll('.metric-el').remove();
  }

  initLineGenerator(): void {
    this.lineGenerator = d3.line()
      .x(d => this.state.xScale(d[0]))
      .y(d => this.state.yScale(d[1]));
  }

  renderLine(serie: Serie): void {
    this.metricContainer
      .append('path')
      .datum(serie.datapoints)
      .attr('class', `metric-path-${serie.idx} metric-el ${serie.class}`)
      .attr('fill-opacity', 0)
      .attr('stroke', serie.color)
      .attr('stroke-width', 1)
      .attr('stroke-opacity', 0.7)
      .attr('pointer-events', 'none')
      .attr('d', this.lineGenerator);
  }

  onMouseMove(): void {}
  onMouseOver(): void {}
  onMouseOut(): void {}
  onMouseClick(): void {}
  renderSharedCrosshair(values): void {}
  hideSharedCrosshair(): void {}
}

export { DemoPod };
