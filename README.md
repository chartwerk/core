


# Chartwerk Core

Repo contains the core code of chartwerk project: abstract classes, rendering system, basic components.
See ChartwerkPod to see what is parent for all chartwerk pods and get involved into development.
Everything can be overwritted.

## Plugin contains:
- SVG container.
- Series and Options models with defaults.
- State model to control charts changes.
- Overlay container to handle all events.
- Zoom events controller.
- Axes, with ticks and labels.
- Grid, with separate behavior from axis.
- Legend, which can hide metrics.
- Crosshair.


## Declare
```js
const pod = new ChartwerkPod(
  document.getElementById('chart-div'),
  series,
  options,
);
pod.render();
```

## Series

Series is a list of metrics with datapoints and specific config working for each serie.
series = Serie[]

- `datapoints` - metric data for rendering.
```js
datapoints = [number, number][]; // 0 index for X, 1 index for Y 
```

- `target` - id of metric. Required param, should be unique.
```js
target: string;
```

## Options:

Options is a config working for whole chart and metrics.
All options are optional.

- `margin` — chart container positioning;
```js
margin = {
  top: number,
  right: number,
  bottom: number,
  left: number,
}
```

- `colors`: array of metrics colors (should be equal or greater than series length);
```js
['red', 'blue', 'green']
```

- `labelFormat`: labels for axes.
```
{
  xAxis: string;
  yAxis: string;
}
```
for example:
```js
{
  xAxis: 'Time';
  yAxis: 'Value';
}
```

- `bounds`: specify which metrics should be rendered as confidence. (TODO: move to `@chartwerk/line-chart`)
`$__metric_name` macro can be used here. It will be replaced with each metric's name to find it's bound.
```
{
  upper: string;
  lower: string;
}
```

for example:
metric names: 'serie', 'serie upper_bound', 'serie lower_bound'
```js
bounds={
  upper: '$__metric_name upper_bound';
  lower: '$__metric_name lower_bound';
}
```
'serie upper_bound', 'serie lower_bound' metrics will be rendered as `serie` metric confidence;

- `timeRange`: time range in timestamps
```
{
  from: number;
  to: number;
}
```
for example:
```js
{
  from: 1582770000000;
  to: 1582810000000;
}
```

- `eventsCallbacks`: event callbacks

```js
{
  zoomIn: (range: [number, number]) => void,
  zoomOut: (center: number) => void,
  mouseMove: (evt: any) => void,
  mouseOut: () => void,
  onLegendClick: (idx: number) => void
}
```
