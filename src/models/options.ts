import {
  Options,
  GridOptions, AxesOptions, AxisFormat,
  CrosshairOptions, CrosshairOrientation,
  ZoomEvents, MouseZoomEvent, MousePanEvent, DoubleClickEvent, ScrollZoomEvent, ScrollPanEvent,
  ScrollPanOrientation, ScrollPanDirection, PanOrientation, KeyEvent, BrushOrientation,
  Margin, TimeFormat, AxisRange, RenderComponent,
} from '../types';

import lodashDefaultsDeep from 'lodash/defaultsDeep';
import lodashCloneDeep from 'lodash/cloneDeep';
import has from 'lodash/has';


const DEFAULT_TICK_COUNT = 4;
const DEFAULT_SCROLL_PAN_STEP = 50;
const DEFAULT_GRID_TICK_COUNT = 5;

const DEFAULT_GRID_OPTIONS: GridOptions = {
  x: {
    enabled: true,
    ticksCount: DEFAULT_GRID_TICK_COUNT,
  },
  y: {
    enabled: true,
    ticksCount: DEFAULT_GRID_TICK_COUNT,
  },
}

const DEFAULT_AXES_OPTIONS: AxesOptions = {
  x: {
    isActive: true,
    ticksCount: DEFAULT_TICK_COUNT,
    format: AxisFormat.TIME
  },
  y: {
    isActive: true,
    ticksCount: DEFAULT_TICK_COUNT,
    format: AxisFormat.NUMERIC
  },
  y1: {
    isActive: false,
    ticksCount: DEFAULT_TICK_COUNT,
    format: AxisFormat.NUMERIC
  }
}

const DEFAULT_CROSSHAIR_OPTIONS = {
  orientation: CrosshairOrientation.VERTICAL,
  color: 'gray',
}

const DEFAULT_MARGIN: Margin = { top: 30, right: 20, bottom: 20, left: 30 };

export const CORE_DEFAULT_OPTIONS: Options = {
  zoomEvents: {
    mouse: {
      zoom: {
        isActive: true,
        keyEvent: KeyEvent.MAIN,
        orientation: BrushOrientation.HORIZONTAL
      },
      pan: {
        isActive: true,
        keyEvent: KeyEvent.SHIFT,
        orientation: PanOrientation.HORIZONTAL
      },
      doubleClick: {
        isActive: true,
        keyEvent: KeyEvent.MAIN,
      },
    },
    scroll: {
      zoom: {
        isActive: true,
        keyEvent: KeyEvent.MAIN,
        orientation: PanOrientation.BOTH,
      },
      pan: {
        isActive: false,
        keyEvent: KeyEvent.SHIFT,
        panStep: DEFAULT_SCROLL_PAN_STEP,
        orientation: ScrollPanOrientation.HORIZONTAL,
        direction: ScrollPanDirection.BOTH,
      },
    },
  },
  axis: DEFAULT_AXES_OPTIONS,
  grid: DEFAULT_GRID_OPTIONS,
  crosshair: DEFAULT_CROSSHAIR_OPTIONS,
  renderLegend: true,
  margin: DEFAULT_MARGIN,
  eventsCallbacks: {},
}

export class CoreOptions<O extends Options> {
  _options: O;

  constructor(options: O, private _podDefaults?: Partial<O>) {
    this.setOptions(options);
  }

  public updateOptions(options: O): void {
    this.setOptions(options);
  }

  protected setOptions(options: O): void {
    this._options = lodashDefaultsDeep(lodashCloneDeep(options), this.getDefaults());
  }

  // this getter can be overrited in Pod
  protected getDefaults(): Partial<O> {
    return lodashDefaultsDeep(this._podDefaults, CORE_DEFAULT_OPTIONS);
  }

  get allOptions(): O {
    return this._options;
  }

  get grid(): GridOptions {
    return this._options.grid;
  }

  get axis(): AxesOptions {
    return this._options.axis;
  }

  get crosshair(): CrosshairOptions {
    return this._options.crosshair;
  }

  get margin(): Margin {
    return this._options.margin;
  }

  // events
  get allEvents(): ZoomEvents {
    return this._options.zoomEvents;
  }

  get mouseZoomEvent(): MouseZoomEvent {
    return this._options.zoomEvents.mouse.zoom;
  }

  get mousePanEvent(): MousePanEvent {
    return this._options.zoomEvents.mouse.pan;
  }

  get doubleClickEvent(): DoubleClickEvent {
    return this._options.zoomEvents.mouse.doubleClick;
  }

  get scrollZoomEvent(): ScrollZoomEvent {
    return this._options.zoomEvents.scroll.zoom;
  }

  get scrollPanEvent(): ScrollPanEvent {
    return this._options.zoomEvents.scroll.pan;
  }

  // event callbacks
  callbackRenderStart(): void {
    if(has(this._options.eventsCallbacks, 'renderStart')) {
      this._options.eventsCallbacks.renderStart();
    }
  }

  callbackRenderEnd(): void {
    if(has(this._options.eventsCallbacks, 'renderEnd')) {
      this._options.eventsCallbacks.renderEnd();
    }
  }

  callbackComponentRenderEnd(part: RenderComponent): void {
    if(has(this._options.eventsCallbacks, 'componentRenderEnd')) {
      this._options.eventsCallbacks.componentRenderEnd(part);
    }
  }

  callbackLegendClick(idx: number): void {
    if(has(this._options.eventsCallbacks, 'onLegendClick')) {
      this._options.eventsCallbacks.onLegendClick(idx);
    }
  }

  callbackLegendLabelClick(idx: number): void {
    if(has(this._options.eventsCallbacks, 'onLegendLabelClick')) {
      this._options.eventsCallbacks.onLegendLabelClick(idx);
    }
  }

  callbackPanning(event: { ranges: AxisRange[], d3Event: any }): void {
    if(has(this._options.eventsCallbacks, 'panning')) {
      this._options.eventsCallbacks.panning(event);
    }
  }

  callbackPanningEnd(ranges: AxisRange[]): void {
    if(has(this._options.eventsCallbacks, 'panningEnd')) {
      this._options.eventsCallbacks.panningEnd(ranges);
    }
  }

  callbackZoomIn(ranges: AxisRange[]): void {
    if(has(this._options.eventsCallbacks, 'zoomIn')) {
      this._options.eventsCallbacks.zoomIn(ranges);
    }
  }

  callbackZoomOut(centers: { x: number, y: number }): void {
    if(has(this._options.eventsCallbacks, 'zoomOut')) {
      this._options.eventsCallbacks.zoomOut(centers);
    }
  }

  callbackSharedCrosshairMove(event: { datapoints, eventX, eventY }): void {
    if(has(this._options.eventsCallbacks, 'sharedCrosshairMove')) {
      this._options.eventsCallbacks.sharedCrosshairMove(event);
    }
  }

  callbackMouseMove(event): void {
    if(has(this._options.eventsCallbacks, 'mouseMove')) {
      this._options.eventsCallbacks.mouseMove(event);
    }
  }

  callbackMouseOut(): void {
    if(has(this._options.eventsCallbacks, 'mouseOut')) {
      this._options.eventsCallbacks.mouseOut();
    }
  }

  callbackMouseClick(event): void {
    if(has(this._options.eventsCallbacks, 'mouseClick')) {
      this._options.eventsCallbacks.mouseClick(event);
    }
  }
}
