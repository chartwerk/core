import { GridOptions, SvgElParams } from '../types';

import * as d3 from 'd3';


// Grid Class - is a core component, which can be a separate Pod in the future. (but not in current Pod terminology)
// All components have construcor with required args: svg element which will be filled with this component and options for it.
// All compoтents have a reqiured method "render", which will be called in core costructor. <- this solution is temporary.
// svgElement should be a separate class with its own height, width, xScale, yScale params to avoid SvgElParams as argument.
// We have a general problem with passing d3 as argument everywhere. Fix it, and remove from arg in constructor here.
export class Grid {
  constructor(
    private _svgEl: d3.Selection<SVGElement, unknown, null, undefined>,
    private _svgElParams: SvgElParams,
    protected gridOptions: GridOptions,
  ) {}

  public render(): void {
    this.clear();
  
    this.renderGridLinesX();
    this.renderGridLinesY();
  }

  clear(): void {
    // TODO: temporary. Move out of here
    this._svgEl.selectAll('.grid').remove();
  }

  renderGridLinesX(): void {
    if(!this.gridOptions.x.enabled) {
      return;
    }
    this._svgEl
      .append('g')
      .attr('transform', `translate(0,${this._svgElParams.height})`)
      .attr('class', 'grid x-grid')
      .style('pointer-events', 'none')
      .call(
        d3.axisBottom(this._svgElParams.xScale)
          .ticks(this.gridOptions.x.ticksCount)
          .tickSize(-this._svgElParams.height)
          .tickFormat(() => '')
      );
  }

  renderGridLinesY(): void {
    if(!this.gridOptions.y.enabled) {
      return;
    }
    this._svgEl
      .append('g')
      .attr('class', 'grid y-grid')
      .style('pointer-events', 'none')
      .call(
        d3.axisLeft(this._svgElParams.yScale)
          .ticks(this.gridOptions.y.ticksCount)
          .tickSize(-this._svgElParams.width)
          .tickFormat(() => '')
      );
  }
}
