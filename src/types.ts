import * as d3 from 'd3';

export type Margin = { top: number, right: number, bottom: number, left: number };
export type Timestamp = number;

export type Serie = {
  target: string,
  datapoints: [Timestamp, number][],
  idx?: number,
  alias?: string,
  visible?: boolean,
  color?: string,
  class?: string,
  yOrientation?: yAxisOrientation,
};
// TODO: move some options to line-chart
export type Options = {
  margin?: Margin;
  eventsCallbacks?: {
    zoomIn?: (range: AxisRange[]) => void,
    panning?: (event: { ranges: AxisRange[], d3Event: any }) => void,
    panningEnd?: (range: AxisRange[]) => void,
    zoomOut?: (centers: {x: number, y: number}) => void,
    mouseMove?: (evt: any) => void,
    mouseClick?: (evt: any) => void,
    mouseOut?: () => void,
    onLegendClick?: (idx: number) => void,
    onLegendLabelClick?: (idx: number) => void,
    contextMenu?: (evt: any) => void, // the same name as in d3.events
    sharedCrosshairMove?: (event: any) => void,
    renderStart?: () => void,
    renderEnd?: () => void,
    componentRenderEnd?: (part: RenderComponent) => void,
  };
  axis?: AxesOptions;
  grid?: GridOptions;
  crosshair?: CrosshairOptions;
  zoomEvents?: ZoomEvents;
  renderTicksfromTimestamps?: boolean;
  renderLegend?: boolean;
};

export type GridOptions = {
  x?: {
    enabled?: boolean;
    ticksCount?: number;
  },
  y?: {
    enabled?: boolean;
    ticksCount?: number;
  },
}

export type AxesOptions = {
  x?: AxisOption,
  y?: AxisOption,
  y1?: AxisOption
}

export type AxisOption = {
  isActive?: boolean;
  ticksCount?: number;
  format?: AxisFormat;
  range?: [number, number];
  invert?: boolean;
  label?: string;
  valueFormatter?: (value: number, i: number) => string;
  colorFormatter?: (value: number, i: number) => string;
}

export type CrosshairOptions = {
  orientation?: CrosshairOrientation;
  color?: string;
}

export type AxisRange = [number, number] | undefined;
export type VueOptions = Omit<Options, 'eventsCallbacks'>;

export enum TimeFormat {
  SECOND = 'second',
  MINUTE = 'minute',
  HOUR = 'hour',
  DAY = 'day',
  MONTH = 'month',
  YEAR = 'year'
}

export enum BrushOrientation {
  VERTICAL = 'vertical',
  HORIZONTAL = 'horizontal',
  RECTANGLE = 'rectangle',
  SQUARE = 'square'
}

export enum PanOrientation {
  VERTICAL = 'vertical',
  HORIZONTAL = 'horizontal',
  BOTH = 'both',
}

export enum ScrollPanOrientation {
  VERTICAL = 'vertical',
  HORIZONTAL = 'horizontal',
}

export enum ScrollPanDirection  {
  FORWARD = 'forward',
  BACKWARD = 'backward',
  BOTH = 'both',
}

export enum AxisFormat {
  TIME = 'time',
  NUMERIC = 'numeric',
  STRING = 'string',
  CUSTOM = 'custom'
}

export enum CrosshairOrientation {
  VERTICAL = 'vertical',
  HORIZONTAL = 'horizontal',
  BOTH = 'both'
}

export type SvgElementAttributes = {
  x: number,
  y: number,
  width: number,
  height: number
}

export enum KeyEvent {
  MAIN = 'main',
  SHIFT = 'shift'
}

// allow series values to affect a specific axis
export enum xAxisOrientation {
  TOP = 'top',
  BOTTOM = 'bottom',
  BOTH = 'both'
}
export enum yAxisOrientation {
  LEFT = 'left',
  RIGHT = 'right',
}
export type SvgElParams = {
  height: number;
  width: number;
  xScale: d3.ScaleLinear<number, number>;
  yScale: d3.ScaleLinear<number, number>;
}

export type ZoomEvents = {
  mouse?: {
    zoom?: MouseZoomEvent;
    pan?: MousePanEvent;
    doubleClick?: DoubleClickEvent;
  },
  scroll?: {
    zoom?: ScrollZoomEvent;
    pan?: ScrollPanEvent;
  }
}

export type MouseZoomEvent = { // same as brush
  isActive?: boolean;
  keyEvent?: KeyEvent; // main(or base, or smth) / shift / alt / etc
  orientation?: BrushOrientation; // to BrushOrientation: vertical, horizaontal, square, rectange
}

export type MousePanEvent = { // same as brush
  isActive?: boolean;
  keyEvent?: KeyEvent; // main(or base, or smth) / shift / alt / etc
  orientation?: PanOrientation;
}

export type DoubleClickEvent = {
  isActive: boolean;
  keyEvent?: KeyEvent;
}

export type ScrollZoomEvent = {
  isActive?: boolean;
  keyEvent?: KeyEvent;
  orientation?: PanOrientation; // TODO: rename
}

export type ScrollPanEvent = {
  isActive?: boolean;
  keyEvent?: KeyEvent;
  panStep?: number;
  orientation?: ScrollPanOrientation;
  direction?: ScrollPanDirection;
}

export enum RenderComponent {
  CLIP_PATH = 'clipPath',
  OVERLAY = 'overlay',
  AXES = 'axes',
  GRID = 'grid',
  CROSSHAIR = 'crosshair',
  METRICS_CONTAINER = 'metricsContainer',
  LEGEND = 'legend',
}