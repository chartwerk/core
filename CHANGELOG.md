# Changelog

## 0.2.0
* Change order of time\value in `TimeSerie` : now it's `[time, value][]` instead of `[value, time][]`
